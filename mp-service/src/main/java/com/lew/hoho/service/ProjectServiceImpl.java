package com.lew.hoho.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.lew.hoho.entity.Project;
import com.lew.hoho.mapper.ProjectDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Service(timeout = 10000,version = "1.0.0")
public class ProjectServiceImpl implements IProjectService {

    @Autowired
    private ProjectDao  projectDao;

    @Override
    public Project queryById(String id) {
        return projectDao.queryById(id);
    }

    @Override
    public List<Project> queryAllByLimit(int offset, int limit) {
        return projectDao.queryAllByLimit(offset,limit);
    }

    @Override
    public List<Project> queryAll(Project project) {
        return projectDao.queryAll(project);
    }

    @Override
    public int insert(Project project) {
        return projectDao.insert(project);
    }

    @Override
    public int update(Project project) {
        return projectDao.update(project);
    }

    @Override
    public int deleteById(String id) {
        return projectDao.deleteById(id);
    }
}
