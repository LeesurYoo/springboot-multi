package com.lew.hoho.service;

import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

@Component
@Service(timeout = 60000,version = "1.0.0")
public class DubboServiceImpl implements DubboService {
    /**
     *
     * @param name
     * @return
     */
    @Override
    public String getName(String name) {
        return "hello" + name;
    }

    /**
     *
     * @return
     */
    @Override
    public int addUser() {
        return 111;
    }
}
