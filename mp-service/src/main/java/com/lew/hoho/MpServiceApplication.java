package com.lew.hoho;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = MpServiceApplication.class)
@EnableDubbo
public class MpServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(MpServiceApplication.class, args);
    }

}
