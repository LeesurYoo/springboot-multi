package com.lew.hoho.controller;

import com.lew.hoho.dubboService.ProjectDubboService;
import com.lew.hoho.entity.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    ProjectDubboService projectDubboService;


    /**
     * 根据项目id查询项目实体
     *
     * @param id
     * @return
     */
    @GetMapping("/query/{id}")
    @ResponseBody
    public Project queryById(@PathVariable("id") String id) {
        return projectDubboService.queryById(id);
    }

    /**
     * 分页查询项目列表
     *
     * @param offset
     * @param limit
     * @return
     */
    @GetMapping("/query/all/limit")
    @ResponseBody
    public List<Project> queryByLimit(int offset, int limit) {
        return projectDubboService.queryByLimit(offset, limit);
    }

    /**
     * 根据某一个字段查询实体类Project
     *
     * @param project
     * @return
     */
    @GetMapping("/find/property")
    @ResponseBody
    public List<Project> findProject(Project project) {
        return projectDubboService.findProject(project);
    }

}
