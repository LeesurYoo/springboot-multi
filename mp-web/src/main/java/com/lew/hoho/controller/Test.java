package com.lew.hoho.controller;

import com.lew.hoho.dubboService.DubboTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class Test {

    @Autowired
    private DubboTestService  dubboTestService;

    @GetMapping("/demo")
    @ResponseBody
    public String test() {
        return "hello";
    }


   @GetMapping("/dubbo/test")
    @ResponseBody
    public String getName(String name) {
        return dubboTestService.getName(name);
    }
}
