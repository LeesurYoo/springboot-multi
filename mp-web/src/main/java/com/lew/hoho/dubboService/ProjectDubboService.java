package com.lew.hoho.dubboService;

import com.alibaba.dubbo.config.annotation.Reference;
import com.lew.hoho.entity.Project;
import com.lew.hoho.service.IProjectService;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class ProjectDubboService {

    @Reference(version = "1.0.0")
    public IProjectService projectService;

    /**
     * 根据id查询项目
     *
     * @param id
     * @return
     */
    public Project queryById(String id) {
        return  projectService.queryById(id);
    }

    /**
     * 分页查询项目
     *
     * @param offset
     * @param limit
     * @return
     */
    public List<Project> queryByLimit(int offset, int limit) {
        return projectService.queryAllByLimit(offset, limit);
    }

    /**
     * 根据某一个字段查询project实体对象
     * @param project
     * @return
     */
    public  List<Project>  findProject(Project project){
        return  projectService.queryAll(project);
    }
}
