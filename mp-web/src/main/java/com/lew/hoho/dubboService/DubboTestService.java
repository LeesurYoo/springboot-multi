package com.lew.hoho.dubboService;

import com.alibaba.dubbo.config.annotation.Reference;
import com.lew.hoho.service.DubboService;
import org.springframework.stereotype.Service;

@Service
public class DubboTestService {

    @Reference(version = "1.0.0")
    private DubboService dubboService;

    public String getName(String name) {
        return dubboService.getName(name);
    }
}
