package com.lew.hoho.entity;

import java.sql.Timestamp;
import java.util.Date;
import java.io.Serializable;

/**
 * 项目表(Project)实体类
 *
 * @author makejava
 * @since 2019-04-17 14:20:43
 */
public class Project implements Serializable {
    private static final long serialVersionUID = -20012442368716937L;
    //项目ID
    private String id;
    //项目代号
    private String projectNo;
    //项目名称
    private String projectName;
    //状态：0-停用，1启用
    private Byte status;
    //创建时间
    private Date createTime;
    //更新时间
    private Date updateTime;
    //创建人
    private String creator;
    //更新人
    private String updateUser;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectNo() {
        return projectNo;
    }

    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }
}